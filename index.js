// Function parameters are variables that wait for a value from an argument in the function invocation

function sayMyName(name){
	console.log('My name ' + name)

}
// Function arguments are values like strings, numbers, etc. that you can pass onto the function you are invoking
sayMyName('Slim Shady')

// You can re-use functions by invoking them at any time or any part of your code. Make sure you have declared them first.

sayMyName('Quentin Tarantino')
sayMyName ('Lars Ulrich')

// You can also pass variables as arguments to a function
let myName = 'Earl'
sayMyName(myName)

// You can use arguments and parameters to make the data inside your function dynamic

function sumOfTwoNumbers(firstNumber,secondNumber){
	let sum = 0
	sum = firstNumber + secondNumber
	console.log(sum)
}

sumOfTwoNumbers(10,15)
sumOfTwoNumbers(99,1)
sumOfTwoNumbers(50,20)

// You can pass a function as an argument for another function. Do not pass that function with parentheses, only with its function name
function argumentFunction(){
	console.log('This is a function that is passed as an argument.')
}

function parameterFunction(argumentFunction){
	argumentFunction()
}

parameterFunction(argumentFunction)

// REAL WORLD APPLICATION
/*Imagine a product page where you have an "add to cart" button wherein you have the button element itself displayed on the page BUT you dont have the functionality of it yet. This is where passing a function as an argument comes in, in order for you to add or have access to a function to add functionality to your button once it is clicked*/

function addProductToCart(){
	console.log('Click me to add product to cart')
}
let addToCartBtn = document.querySelector('#add-to-cart-btn')

addToCartBtn.addEventListener('click', addProductToCart)

// if you add an extra or if you lack an argument, javascript wont throw an error at you, instead it will ignore the extra argument and turn the lacking parameter into undefined

function displayFullName(firstName, middleInitial, lastName, extraText){
	console.log('Your full name is: ' + firstName + ' ' + middleInitial + ' ' +lastName + ' ' + extraText )
}
// displayFullName('Earl', 'D', 'Diaz')
displayFullName('Johnny', 'B', 'Goode', 'Yeah')


// STRING INTERPOLATION - instead of regular concatination, we can "iterpolate" or inject the variables within the string itself
function displayMovieDetails(title, synopsis, director){
	console.log(`The movie is titled ${title}`)
	console.log(`The movie's synopsis is ${synopsis}`)
	console.log(`The movie director is ${director}`)
}

displayMovieDetails('Django Unchained', 'Racism', 'Quentin Tarantino')

// Return statement/syntax is the final step of any function. It assigns whatever value you put after it as the value of the function upon invoking it

// note: return statements should ALWAYS come last in the function scope
// note: any statement after the return statement will be ignored by the function
function displayPlayerDetails(name, age, playerClass){
	// console.log(`Player name: ${name}`)
	// console.log(`Player age: ${age}`)
	// console.log(`Player class: ${playerClass}`)
	let playerDetails = `Name: ${name}, Age: ${age}, Class: ${playerClass}`
	return playerDetails
}
// displayPlayerDetails('Luka Doncic', '22', 'forward')
console.log(displayPlayerDetails('Luka', 22, 'forward'))